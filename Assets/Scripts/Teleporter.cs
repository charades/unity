﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {
    public GameObject exitNode;

    private float delta = 0.08f;
    public bool faceNorthOnExit = true;

	void OnTriggerEnter2D(Collider2D other)
    {
        if (!faceNorthOnExit)
            delta *= -1;

        other.GetComponent<Transform>().position = exitNode.GetComponent<Transform>().position + new Vector3(0, delta, 0);
        
    }
}
