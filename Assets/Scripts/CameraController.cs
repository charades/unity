﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public GameObject player;
    public GameObject UIMainPanel;
	// Update is called once per frame
	void Update () {
        this.transform.Translate(new Vector3(player.transform.position.x - this.transform.position.x, UIMainPanel.GetComponent<RectTransform>().sizeDelta.y/400 + player.transform.position.y - this.transform.position.y, 0));
	}
}
