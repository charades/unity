﻿using UnityEngine;
using System.Collections;
using System;

public class MovingProjectile : MonoBehaviour {

    public float speed;
    private Rigidbody2D rb2D;
    public Vector2 Direction { get; set; }
    public Sprite pointingUp;
    public Sprite pointingLeft;
    public Sprite pointingRight;
    public Sprite pointingDown;

    public bool Alive { get; set; }


    void Start()
    {
       rb2D = GetComponent<Rigidbody2D>();
    }

    void OnEnable()
    {
        char wasd = getProjectileParentDirection();
        if (wasd == 'w')
        {
            Direction = new Vector2(0f, 0.01f);
            transform.GetComponent<SpriteRenderer>().sprite = pointingUp;
        }
        if (wasd == 'a')
        {
            Direction = new Vector2(-0.01f, 0f);
            transform.GetComponent<SpriteRenderer>().sprite = pointingLeft;
        }
        if (wasd == 's')
        {
            Direction = new Vector2(0f, -0.01f);
            transform.GetComponent<SpriteRenderer>().sprite = pointingDown;
        }
        if (wasd == 'd')
        {
            Direction = new Vector2(0.01f, 0f);
            transform.GetComponent<SpriteRenderer>().sprite = pointingRight;
        }

        Alive = true;
    }

    char getProjectileParentDirection()
    {
        return transform.parent.GetComponent<CharacterController>().getCurrentMovementInput();
    }
    void Update()
    {
        if (Alive)
        {
            rb2D.MovePosition(rb2D.position + (Direction * speed));
        }
    }

}
