﻿using UnityEngine;
using System.Collections;

public class RangedAttack : MonoBehaviour {

    public GameObject projectile;

    private const float MINIMUM_WAIT_TIME = 1f;
    private const float MAXIMUM_WAIT_TIME = 2f;
    private const float PROJECTILE_LIVE_TIME = 1f;

    private GameObject projectileInstance;

    private float timeSinceLastAttack;
    private bool isTimeToAttack;
    private float timeUntilNextAttack;

    void Start()
    {
        projectileInstance = (GameObject)Instantiate(projectile);
        projectileInstance.SetActive(false);
        projectileInstance.transform.parent = transform;

        timeSinceLastAttack = Time.time;
        timeUntilNextAttack = Random.Range(MINIMUM_WAIT_TIME, MAXIMUM_WAIT_TIME);
    }
	
    void Update()
    {
        
        if (Time.time - timeSinceLastAttack >= timeUntilNextAttack)
        {
            isTimeToAttack = true;
            timeUntilNextAttack = Random.Range(MINIMUM_WAIT_TIME, MAXIMUM_WAIT_TIME);
        }

        if (Time.time - timeSinceLastAttack >= PROJECTILE_LIVE_TIME)
            projectileInstance.SetActive(false);
        

    }
    void FixedUpdate()
    {
        if (isTimeToAttack)
        {
            LaunchRangedAttack();
            isTimeToAttack = false;
            timeSinceLastAttack = Time.time;
        }
    }
    void LaunchRangedAttack()
    {
        projectileInstance.transform.position = transform.position;
        projectileInstance.transform.rotation = transform.rotation;
        projectileInstance.SetActive(true);
    }
}
