﻿using UnityEngine;
using System.Collections;

public class PlaySFXOnAnimation : StateMachineBehaviour {

    public AudioClip clipToPlay;
    private AudioSource audioSource;
	//OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        audioSource = animator.GetComponent<AudioSource>();
        audioSource.PlayOneShot(clipToPlay);
	}

}
