﻿using UnityEngine;
using System.Collections;

public class DamageableMonster : MonoBehaviour {

    public float health;
    public AudioClip Hit_SFX;
    Animator damageableObjectAnimator;

    void Start()
    {
        damageableObjectAnimator = GetComponent<Animator>();
    }

    void apply1Damage()
    {
        health--;
        damageableObjectAnimator.GetComponent<AudioSource>().PlayOneShot(Hit_SFX);
    }
	// Update is called once per frame
	void Update () {
        if (health <= 0)
        {
            damageableObjectAnimator.SetTrigger("Death");
        }
	}
}
