﻿using UnityEngine;
using System.Collections;

public class Death : StateMachineBehaviour {

    public AudioClip Death_SFX;
	  // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.GetComponent<AudioSource>().PlayOneShot(Death_SFX);
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Destroy(animator.gameObject);
	}

}
