﻿using UnityEngine;
using System.Collections;

public class MonsterController : CharacterController {
    public bool TimeToMove { get; set; }
    public float TimeOfLastMovement { get; set; }
    public float MovementTime { get; set; }

    public float test;

    override protected void Start()
    {
        base.Start();
        TimeToMove = true;
    }
    override protected void getMovementInput()
    {
        if (TimeToMove) {
            TimeOfLastMovement = Time.time;
            MovementTime = Random.Range(0.3f, 0.5f);
        }

        if (TimeToMove)
        {
            float random = Random.Range(0f, 0.4f);

            if (random >= 0f && random < 0.1f)
                w = true;
            else if (random >= 0.1f && random < 0.2f)
                a = true;
            else if (random >= 0.2f && random < 0.3f)
                s = true;
            else if (random >= 0.3f && random <= 0.4f)
                d = true;

            TimeToMove = false;
        }

        if (Time.time - TimeOfLastMovement >= MovementTime)
        {
            TimeToMove = true;
            w = a = s = d = false;
        }
    }
	
}
