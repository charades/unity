﻿using UnityEngine;
using System.Collections;

public class Sword1Damage : MonoBehaviour {

    public float knockback;
	// Use this for initialization
	void Start () {
        knockback = 1000;
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer != 8)
        {
            float xDistance = (other.transform.position.x - gameObject.GetComponentInParent<Transform>().position.x);
            float yDistance = (other.transform.position.y - gameObject.GetComponentInParent<Transform>().position.y);
            other.collider.SendMessage("apply1Damage");
            other.rigidbody.AddForce(new Vector2(xDistance * knockback, yDistance * knockback));
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer != 8)
        {
            float xDistance = (other.transform.position.x - gameObject.GetComponentInParent<Transform>().position.x);
            float yDistance = (other.transform.position.y - gameObject.GetComponentInParent<Transform>().position.y);
            other.SendMessage("apply1Damage");
            other.GetComponent<Rigidbody2D>().AddForce(new Vector2(xDistance * knockback, yDistance * knockback));
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
