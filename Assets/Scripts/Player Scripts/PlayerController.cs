﻿using UnityEngine;
using System.Collections;

public class PlayerController : CharacterController {

    new protected void Update()
    {
        base.Update();
        resetAttackState();
        getAttackState();
    }

    private void getAttackState()
    {
        if (Input.GetMouseButtonDown(0))
            characterAnimator.SetTrigger("Attack");
        if (Input.GetKeyDown(KeyCode.Space))
            characterAnimator.SetTrigger("Attack");
    }

    private void resetAttackState()
    {
        characterAnimator.ResetTrigger("Attack");
    }
    override protected void getMovementInput()
    {
        w = Input.GetKey(KeyCode.W);
        a = Input.GetKey(KeyCode.A);
        s = Input.GetKey(KeyCode.S);
        d = Input.GetKey(KeyCode.D);
    }
}
