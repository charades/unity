﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{

    public AudioClip Hurt;
    public AudioClip Block_SFX;
    private AudioSource audioSource;

    public float hp;
    public float knockback;
    public float hurtFlashTime;
    public float blockReboundForce;

    private bool invincible;

    private Animator playerAnimator;
    public SpriteRenderer playerSprite;

    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = Hurt;
        playerSprite = GetComponent<SpriteRenderer>();
        invincible = false;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer != 8 && !invincible)
        {
            hp -= other.gameObject.GetComponent<Damage>().damage;
            playHurtSound();
            applyKnockbackOnPlayer(knockback, other.transform);
            invincible = true;
            StartCoroutine(flashPlayerToRed());
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 11 && !invincible)
        {
            float colliderXDirection = other.GetComponent<MovingProjectile>().Direction.x;
            float colliderYDirection = other.GetComponent<MovingProjectile>().Direction.y;
            if (!isPlayerFacingCollider(colliderXDirection, colliderYDirection)) {
                hp--;
                playHurtSound();
                applyKnockbackOnPlayer(knockback * 2, other.transform);
                invincible = true;
                StartCoroutine(flashPlayerToRed());
                other.gameObject.SetActive(false);
            }
            else
            {
                audioSource.PlayOneShot(Block_SFX);
                StartCoroutine(reboundBlockedProjectile(other, colliderXDirection, colliderYDirection));
            }
            
        }
    }

    private bool isPlayerFacingCollider(float colliderXDirection, float colliderYDirection)
    {
        char playerDirection = gameObject.GetComponent<PlayerController>().LastMovementInput;
        return ((colliderXDirection > 0 && playerDirection == 'a') || (colliderXDirection < 0 && playerDirection == 'd') || (colliderYDirection > 0 && playerDirection == 's') || (colliderYDirection < 0 && playerDirection == 'w'));
    }

    IEnumerator reboundBlockedProjectile(Collider2D projectile, float projectileXDirection, float projectileYDirection)
    {
        Rigidbody2D projectileRB2D = projectile.GetComponent<Rigidbody2D>();
        projectile.GetComponent<MovingProjectile>().Alive = false;
        projectileRB2D.AddForce(new Vector2(-projectileXDirection * blockReboundForce, -projectileYDirection * blockReboundForce));
        projectileRB2D.gravityScale = .5f;
        yield return new WaitForSeconds(.4f);
        projectileRB2D.gravityScale = 0;
        projectile.gameObject.SetActive(false);
    }

    IEnumerator flashPlayerToRed()
    {
        for (int i = 0; i < 5; i++)
        {
            playerSprite.material.color = Color.red;
            yield return new WaitForSeconds(hurtFlashTime);
            playerSprite.material.color = Color.white;
            yield return new WaitForSeconds(hurtFlashTime);
        }
        invincible = false;
    }

    private void playHurtSound()
    {
        audioSource.Play();
    }

    private void applyKnockbackOnPlayer(float knockback, Transform other)
    {
        float xDistance = (transform.position.x - other.position.x);
        float yDistance = (transform.position.y - other.position.y);
        GetComponent<Rigidbody2D>().AddForce(new Vector2(xDistance * knockback, yDistance * knockback));
    }

    void Update()
    {
        if (hp <= 0)
            playerAnimator.SetTrigger("Death");
    }
}
