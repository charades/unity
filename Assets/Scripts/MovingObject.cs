﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour {

	public LayerMask blockingLayer;
	public float speed = 10f;
    protected const float MOVEMENT_SCALE = 0.01f;

    protected BoxCollider2D boxCollider;
	protected Rigidbody2D rb2D;

	// Use this for initialization
	protected virtual void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
		rb2D = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate () {
		Move (speed, rb2D);
	}

    protected abstract void Move(float speed, Rigidbody2D rb2D);

	
}
