﻿using UnityEngine;
using System.Collections;
using System;

public abstract class CharacterController : MovingObject {

    protected Animator characterAnimator;
    private int horizontalMovementDirection;
    private int verticalMovementDirection;

    public bool w { get; set; }
    public bool a { get; set; }
    public bool s { get; set; }
    public bool d { get; set; }
    public char LastMovementInput;

    protected override void Start () {
		base.Start ();
		characterAnimator = GetComponent<Animator> ();
        w = false;
        s = false;
        a = false;
        d = false;
        horizontalMovementDirection = 0;
        verticalMovementDirection = 0;
    }

    protected override void Move(float speed, Rigidbody2D rb2D)
    {
        resetButtonPressed();
        getMovementInput();
        setMovementDirection();
        setVelocity(new Vector2(horizontalMovementDirection * speed, verticalMovementDirection * speed));
    }

    protected void Update()
    {
        resetAnimationParameters();
        setAnimationParameters();      
    }

    private void resetButtonPressed()
    {
        horizontalMovementDirection = 0;
        verticalMovementDirection = 0;
    }

    private void resetAnimationParameters()
    {
        characterAnimator.ResetTrigger("MovingHorizontal");
        characterAnimator.ResetTrigger("MovingVertical");
        characterAnimator.SetFloat("VerticalSpeed", 0f);
        characterAnimator.SetFloat("HorizontalSpeed", 0f);

    }

    /// <summary>
    /// Return a char ('w', 'a', 's', 'd') representing
    /// the current direction the character is going.
    /// Returns x if it is idle (not trying to go anywhere).
    /// </summary>
    /// <returns>'w' = up, 'a' = left, 's' = down, 'd' = right, 'x' = idle</returns>
    public char getCurrentMovementInput()
    {
        char input = 'x';

        if (w)
        {
            input = 'w';
            
        }

        if (a)
        {
            input = 'a';

        }

        if (s)
        {
            input = 's';

        }

        if (d)
        {
            input = 'd';

        }

        return input;
    }

    abstract protected void getMovementInput();



    private void setMovementDirection()
    {
        if (w)
        {
            verticalMovementDirection = 1;
            LastMovementInput = 'w';
        }
        else if (s)
        {
            verticalMovementDirection = -1;
            LastMovementInput = 's';
        }
        else if (a)
        {
            horizontalMovementDirection = -1;
            LastMovementInput = 'a';
        }
        else if (d)
        {
            horizontalMovementDirection = 1;
            LastMovementInput = 'd';
        }
    }

    private void setAnimationParameters()
    {
        if (Math.Abs(rb2D.velocity.y) < Math.Abs(rb2D.velocity.x))
            characterAnimator.SetTrigger("MovingHorizontal");
        else
            if (Math.Abs(rb2D.velocity.y) > Math.Abs(rb2D.velocity.x))
            characterAnimator.SetTrigger("MovingVertical");

        characterAnimator.SetFloat("VerticalSpeed", verticalMovementDirection);
        characterAnimator.SetFloat("HorizontalSpeed", horizontalMovementDirection);
    }
 
    private void setVelocity(Vector2 direction)
    {
        rb2D.AddForce(direction);
        if (!w && !s)
            rb2D.velocity = new Vector2(rb2D.velocity.x, 0);
        if (!a && !d)
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
        this.transform.eulerAngles = new Vector3(0f, 0f, 0f);
    }
    
}
